package com.dmitriy.azarenko.viewlistwithasynktask;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    MyTask myTask;

    ArrayList<Goods> goodsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        goodsList.add(new Goods("Apple"));
        goodsList.add(new Goods("Orange"));
        goodsList.add(new Goods("Milk"));
        goodsList.add(new Goods("Suger"));
        goodsList.add(new Goods("Chokolate"));

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new MyAdapter(this, goodsList));
        myTask = new MyTask();
        myTask.execute();








    }

    class Goods {
        String goods;

        public Goods(String goods) {
            this.goods = goods;
        }

        public String getGoods() {
            return goods;
        }

        public void setGoods(String goods) {

            this.goods = goods;
        }
    }



    class MyTask extends AsyncTask<Void, Integer, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            int counter = 0;
            for (int i = 0; i < 5;i++){
                publishProgress(counter++);



                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }



            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(), "Market is Open", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Market is Closet", Toast.LENGTH_SHORT).show();
        }
    }



        class MyAdapter extends ArrayAdapter<Goods> {

            public MyAdapter(Context context, ArrayList<Goods> objects) {
                super(context, R.layout.list_item, objects);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder holder;
                View rowView = convertView;
                if (rowView == null) {
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    rowView = inflater.inflate(R.layout.list_item, parent, false);
                    holder = new ViewHolder();
                    holder.textView = (TextView) rowView.findViewById(R.id.TextOfList);
                    rowView.setTag(holder);
                } else {
                    holder = (ViewHolder) rowView.getTag();
                }

                Goods goods = getItem(position);
                holder.textView.setText(goods.getGoods());


                return rowView;
            }

            class ViewHolder {

                public TextView textView;

            }


        }







}
